import { NetworkStreamResponse } from '../../proto-typings/social-network_pb';

import DataCase = NetworkStreamResponse.DataCase;

import { graphResponseHandler } from '../services/graph-service';
import {
  addUserResponseHandler,
  deleteUserResponseHandler,
  updateUserResponseHandler,
} from '../services/users-service';
import {
  addFriendResponseHandler,
  deleteFriendshipResponseHandler,
} from '../services/friendship-service';

export type NetworkResponseRoute = {
  [action: number]: (eventData: NetworkStreamResponse) => void;
};

export const networkResponseRouter = (action: number, eventData: NetworkStreamResponse) => {
  const router: NetworkResponseRoute = {
    [DataCase.NETWORK]: (eventData: NetworkStreamResponse) => graphResponseHandler(eventData),
    [DataCase.USER_ADDED]: (eventData: NetworkStreamResponse) => addUserResponseHandler(eventData),
    [DataCase.FRIENDSHIP_ADDED]: (eventData: NetworkStreamResponse) =>
      addFriendResponseHandler(eventData),
    [DataCase.USER_UPDATED]: (eventData: NetworkStreamResponse) =>
      updateUserResponseHandler(eventData),
    [DataCase.USER_DELETED]: (eventData: NetworkStreamResponse) =>
      deleteUserResponseHandler(eventData),
    [DataCase.FRIENDSHIP_DELETED]: (eventData: NetworkStreamResponse) =>
      deleteFriendshipResponseHandler(eventData),
  };
  router[action] && router[action](eventData);
};
