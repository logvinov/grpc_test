import {
  AddUserAction,
  DeleteUserAction,
  NetworkStreamResponse,
  UpdateUserAction,
} from '../../proto-typings/social-network_pb';
import { ISocialNetworkClient } from '../../proto-typings/social-network_grpc_pb';
import { Service } from './index';

export class UsersService extends Service {
  constructor(client: ISocialNetworkClient) {
    super(client);
  }

  createUser = (userMessage: AddUserAction) => {
    return new Promise((resolve, reject) => {
      this.client.addUser(userMessage, (err, result) => {
        if (err) {
          return reject(err);
        }
        return resolve(result);
      });
    });
  };

  deleteUser = (deleteUserRequest: DeleteUserAction) => {
    return new Promise((resolve, reject) => {
      this.client.deleteUser(deleteUserRequest, (err, result) => {
        if (err) {
          return reject(err);
        }
        return resolve(result);
      });
    });
  };

  updateUser = (userMessage: UpdateUserAction) => {
    return new Promise((resolve, reject) => {
      this.client.updateUser(userMessage, (err, result) => {
        if (err) {
          return reject(err);
        }
        return resolve(result);
      });
    });
  };
}

export const addUserResponseHandler = (networkStreamResponse: NetworkStreamResponse) => {
  const userAdded = networkStreamResponse.getUserAdded();
  console.log(`User ${userAdded?.getUsername()} has joined to the Social Network`);
};

export const deleteUserResponseHandler = (networkStreamResponse: NetworkStreamResponse) => {
  const userDeleted = networkStreamResponse.getUserDeleted();
  console.log(`User ${userDeleted?.getUsername()} has been deleted from the Social Network`);
};

export const updateUserResponseHandler = (networkStreamResponse: NetworkStreamResponse) => {
  const userUpdated = networkStreamResponse.getUserUpdated();
  console.log(`User ${userUpdated?.getUsername()} has updated their profile`);
};
