import { ISocialNetworkClient } from '../../proto-typings/social-network_grpc_pb';

export class Service {
  client: ISocialNetworkClient;
  constructor(client: ISocialNetworkClient) {
    this.client = client;
  }
}
