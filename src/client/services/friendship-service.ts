import {
  FriendshipAction,
  NetworkStreamResponse,
} from '../../proto-typings/social-network_pb';
import { ISocialNetworkClient } from '../../proto-typings/social-network_grpc_pb';
import { Service } from './index';

export class FriendshipService extends Service {
  constructor(client: ISocialNetworkClient) {
    super(client);
  }

  addFriendship = (friendshipMessage: FriendshipAction) => {
    return new Promise((resolve, reject) => {
      this.client.addFriend(friendshipMessage, (err, result) => {
        if (err) {
          return reject(err);
        }
        return resolve(result);
      });
    });
  };

  deleteFriend = (friendshipRequest: FriendshipAction) => {
    return new Promise((resolve, reject) => {
      this.client.deleteFriend(friendshipRequest, (err, result) => {
        if (err) {
          return reject(err);
        }
        return resolve(result);
      });
    });
  };
}

export const addFriendResponseHandler = (networkStreamResponse: NetworkStreamResponse) => {
  const friendship = networkStreamResponse.getFriendshipAdded();
  console.log(
    `Users ${friendship?.getUsername()} and ${friendship?.getFriendusername()} now are friends`,
  );
};

export const deleteFriendshipResponseHandler = (networkStreamResponse: NetworkStreamResponse) => {
  const friendDeleted = networkStreamResponse.getFriendshipDeleted();
  console.log(
    `User ${friendDeleted?.getUsername()} has deleted Friend ${friendDeleted?.getFriendusername()} from friend list`,
  );
};
