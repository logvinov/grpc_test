import { NetworkStreamResponse } from '../../proto-typings/social-network_pb';

export const graphResponseHandler = (networkStreamResponse: NetworkStreamResponse) => {
  const graphResponse = networkStreamResponse.getNetwork();
  const graph = graphResponse?.getUsersList().reduce((graph, currentUser) => {
    const userFriends: string[] = [];
    graphResponse?.getFriendshipList().forEach((friendship) => {
      const username = friendship.getUsername();
      const friendUsername = friendship.getFriendusername();
      if (username === currentUser.getUsername()) {
        userFriends.push(friendship.getFriendusername());
      }
      if (friendUsername === currentUser.getUsername()) {
        userFriends.push(friendship.getUsername());
      }
    });
    graph += `${currentUser.getUsername()} --> ${userFriends.join(', ')}\n`;
    return graph;
  }, '');
  console.log(graph || 'Graph is empty at this moment');
};
