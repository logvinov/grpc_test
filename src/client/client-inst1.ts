import { SocialNetworkClient } from '../proto-typings/social-network_grpc_pb';
import * as grpc from 'grpc';
import { Empty, NetworkStreamResponse } from '../proto-typings/social-network_pb';
import { networkResponseRouter } from './router';

const { APP_PORT, APP_HOST } = process.env;

const startClient = async () => {
  const socialNetworkService = new SocialNetworkClient(
    `${APP_HOST}:${APP_PORT}`,
    grpc.credentials.createInsecure(),
  );
  const socialNetworkConnection = socialNetworkService.join(new Empty());

  socialNetworkConnection.on('data', async (data: NetworkStreamResponse) => {
    networkResponseRouter(data.getDataCase(), data);
  });
};

startClient();
