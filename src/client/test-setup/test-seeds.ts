export type SeedUser = {
  username: string;
  name: string;
  bio: string;
  friends: string[];
};

export const USERS_SEEDS = [
  {
    name: 'Leanne Graham',
    username: 'Bret',
    friends: ['Antonette', 'Samantha', 'Karianne'],
    bio:
      'Ipsum et exercitation irure reprehenderit velit aliqua adipisicing cillum do. Laborum sunt officia excepteur in aute aute nisi adipisicing incididunt cupidatat eiusmod culpa. Duis velit aliquip eu aliquip.',
  },
  {
    name: 'Ervin Howell',
    username: 'Antonette',
    friends: ['Samantha', 'Moriah.Stanton'],
    bio:
      'Ullamco non non in ipsum Lorem aute elit nisi irure pariatur. Ullamco anim adipisicing deserunt occaecat sit labore id exercitation est irure dolore Lorem nostrud est. Ad ea ex sunt excepteur aute anim veniam quis exercitation nulla veniam duis. Velit mollit ipsum consectetur deserunt. Est laborum laborum tempor commodo quis. Sunt anim nisi id esse eu commodo irure fugiat. Occaecat ullamco fugiat ad qui.',
  },
  {
    name: 'Clementine Bauch',
    username: 'Samantha',
    friends: ['Karianne', 'Kamren', 'Leopoldo_Corkery', 'Elwyn.Skiles', 'Moriah.Stanton'],
    bio:
      'Eu laborum ex dolore nisi mollit proident in fugiat. Qui enim deserunt do nostrud sunt non enim in qui qui. Dolore pariatur duis id eiusmod ullamco sint minim sunt. Reprehenderit sint eu eu quis commodo qui amet. Sit occaecat commodo esse adipisicing sunt aliquip elit irure. Anim duis ipsum ut excepteur velit velit.',
  },
  {
    name: 'Patricia Lebsack',
    username: 'Karianne',
    friends: [],
    bio:
      'Est magna ea velit elit amet commodo amet esse dolor veniam elit dolor id esse. Do proident amet reprehenderit et. Eu duis deserunt cillum irure tempor enim culpa cupidatat laboris.',
  },
  {
    name: 'Chelsey Dietrich',
    username: 'Kamren',
    friends: ['Bret', 'Elwyn.Skiles', 'Maxime_Nienow', 'Delphine'],
    bio:
      'Aliqua incididunt incididunt enim aliqua id adipisicing. Laboris est cupidatat duis proident magna quis exercitation. Anim deserunt sint nulla id irure amet id consequat reprehenderit dolore occaecat excepteur est exercitation. Ex amet ea occaecat exercitation nisi sunt sunt sunt in ex cillum qui amet enim.',
  },
  {
    name: 'Mrs. Dennis Schulist',
    username: 'Leopoldo_Corkery',
    friends: ['Antonette', 'Bret', 'Delphine'],
    bio:
      'Velit incididunt amet nostrud sint cupidatat voluptate ea nisi quis reprehenderit exercitation aute. Proident labore eu in non laborum deserunt. Esse sit duis et non ullamco nostrud eu amet ad id laboris amet ex. Officia quis labore eu sunt eu. Dolor deserunt anim irure enim sint pariatur deserunt officia qui adipisicing Lorem anim cillum in.',
  },
  {
    name: 'Kurtis Weissnat',
    username: 'Elwyn.Skiles',
    friends: ['Maxime_Nienow', 'Delphine'],
    bio:
      'Anim eiusmod excepteur incididunt occaecat qui sint nulla proident cupidatat. Aute Lorem non qui velit ut fugiat dolor exercitation mollit nulla non tempor. Eiusmod ipsum labore anim anim.',
  },
  {
    name: 'Nicholas Runolfsdottir V',
    username: 'Maxime_Nienow',
    friends: [],
    bio:
      'Do ad sunt ea cupidatat eu tempor consectetur amet aliquip minim incididunt dolor do. Laborum nostrud occaecat nostrud ut sunt voluptate id nisi culpa voluptate. Magna amet sint enim id magna reprehenderit ut reprehenderit laboris. Anim eiusmod incididunt deserunt cillum. Dolor id tempor eu do do culpa ea reprehenderit non. Officia aliqua voluptate labore magna enim aute enim officia sit. Ea tempor enim ullamco ut in esse reprehenderit cupidatat dolore nisi exercitation quis.',
  },
  {
    name: 'Glenna Reichert',
    username: 'Delphine',
    friends: ['Moriah.Stanton'],
    bio:
      'Consequat id veniam labore consectetur id sunt. Non adipisicing nulla minim amet eiusmod exercitation cillum veniam ex duis nulla. Culpa proident est labore consectetur aliquip id.',
  },
  {
    name: 'Clementina DuBuque',
    username: 'Moriah.Stanton',
    friends: ['Karianne', 'Kamren'],
    bio:
      'Deserunt nulla duis non fugiat id anim commodo magna. Minim labore irure occaecat duis consequat quis nulla reprehenderit. Laboris culpa non eiusmod aliquip ut et.',
  },
];
