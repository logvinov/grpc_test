import {
  AddUserAction,
  DeleteUserAction,
  FriendshipAction,
  UpdateUserAction,
} from '../../proto-typings/social-network_pb';
import { UsersService } from '../services/users-service';
import { ISocialNetworkClient } from '../../proto-typings/social-network_grpc_pb';
import { FriendshipService } from '../services/friendship-service';
import { USERS_SEEDS } from '../test-setup/test-seeds';

export const socialNetworkHandler = async (client: ISocialNetworkClient) => {
  try {
    const usersService = new UsersService(client);
    const friendshipService = new FriendshipService(client);

    for (let i = 0; i < USERS_SEEDS.length; i++) {
      const addUserRequest = new AddUserAction();
      addUserRequest.setUsername(USERS_SEEDS[i].username);
      await usersService.createUser(addUserRequest);
    }

    for (let i = 0; i < USERS_SEEDS.length; i++) {
      for (let j = 0; j < USERS_SEEDS[i].friends.length; j++) {
        const addFriendshipRequest = new FriendshipAction();
        addFriendshipRequest.setUsername(USERS_SEEDS[i].username);
        addFriendshipRequest.setFriendusername(USERS_SEEDS[i].friends[j]);
        await friendshipService.addFriendship(addFriendshipRequest);
      }
    }

    const updateUserRequest = new UpdateUserAction();
    updateUserRequest.setUsername('Kamren');
    updateUserRequest.setName('Kamren Salmon');
    updateUserRequest.setBio('Some story from the life of Kamren');

    const deleteFriendship = new FriendshipAction();
    deleteFriendship.setUsername('Kamren');
    deleteFriendship.setFriendusername('Moriah.Stanton');

    const deleteUserRequest = new DeleteUserAction();
    deleteUserRequest.setUsername('Moriah.Stanton');

    await usersService.updateUser(updateUserRequest);

    await friendshipService.deleteFriend(deleteFriendship);

    await usersService.deleteUser(deleteUserRequest);
  } catch (err) {
    console.error(`Error caught: ${err}`);
  }
};
