import socialNetworkHandler from '../handlers/social-network-handler';

import { GRPCServer } from '../utils/grpc-server';
import { ISocialNetworkServer } from '../../proto-typings/social-network_grpc_pb';

export class GRPCRouter {
  server: GRPCServer;
  constructor(server: GRPCServer) {
    this.server = server;
  }
  init() {
    this.server.addService<ISocialNetworkServer>(
      socialNetworkHandler.service,
      socialNetworkHandler.handler,
    );
  }
}
