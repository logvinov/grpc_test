import { GRPCRouter } from '../index';
import { SocialNetworkHandler } from '../../handlers/social-network-handler';

describe('GRPCRouter Class', () => {
  it('should init gRPC Router with services', () => {
    const server = { addService: jest.fn() };
    const grpcRouter = new GRPCRouter(server);
    grpcRouter.init();
    expect(server.addService).toHaveBeenCalledWith(
      expect.anything(),
      expect.any(SocialNetworkHandler),
    );
  });
});
