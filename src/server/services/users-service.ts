import { AlreadyExistError, NotFoundError } from '../errors';
import { UserValidator } from '../validators/user-validator';
import {
  AddUserAction,
  DeleteUserAction,
  Empty,
  NetworkStreamResponse,
  UpdateUserAction,
  UserMessage,
} from '../../proto-typings/social-network_pb';
import { friendshipService } from './friendship-service';

export class UsersService {
  users: Map<string, UserMessage>;
  private static instance: UsersService;
  validator: UserValidator;

  private constructor() {
    this.validator = new UserValidator();
    this.users = new Map();
  }

  static getInstance(): UsersService {
    if (!UsersService.instance) {
      UsersService.instance = new UsersService();
    }

    return UsersService.instance;
  }

  findAll() {
    return this.users;
  }

  findOne(username: string) {
    return this.users.get(username);
  }

  add(user: AddUserAction) {
    this.validator.addUser(user);

    const unixTimestamp = Math.floor(Date.now() / 1000);
    const userAlreadyExist = this.users.get(user.getUsername());

    if (userAlreadyExist) {
      throw new AlreadyExistError(`User ${user.getUsername()} already exists`);
    }
    const userProfile = new UserMessage();
    userProfile.setUsername(user.getUsername());
    userProfile.setCreatedat(unixTimestamp);
    userProfile.setUpdatedat(unixTimestamp);

    this.users.set(user.getUsername(), userProfile);

    const networkStreamResponse = new NetworkStreamResponse();
    networkStreamResponse.setUserAdded(userProfile);

    return {
      event: networkStreamResponse,
      response: new Empty(),
    };
  }

  update(user: UpdateUserAction) {
    this.validator.updateUser(user);

    const userExist = this.users.get(user.getUsername());
    if (!userExist) {
      throw new NotFoundError(`User ${user.getUsername()} not found`);
    }
    const userProfile = new UserMessage();

    userProfile.setUsername(user.getUsername());
    userProfile.setBio(user.getBio() || userExist.getBio());
    userProfile.setName(user.getName() || userExist.getBio());
    userProfile.setCreatedat(userExist.getCreatedat());

    this.users.set(user.getUsername(), userProfile);

    const networkStreamResponse = new NetworkStreamResponse();
    networkStreamResponse.setUserUpdated(userProfile);

    return {
      event: networkStreamResponse,
      response: new Empty(),
    };
  }

  delete(user: DeleteUserAction) {
    this.validator.deleteUser(user);

    const userExist = this.users.get(user.getUsername());
    if (!userExist) {
      throw new NotFoundError(`User ${user.getUsername()} not found`);
    }
    this.users.delete(user.getUsername());

    const userDeleted = new UserMessage();
    userDeleted.setUsername(user.getUsername());

    friendshipService.deleteFriendships(user.getUsername());

    const networkStreamResponse = new NetworkStreamResponse();
    networkStreamResponse.setUserDeleted(userDeleted);

    return {
      event: networkStreamResponse,
      response: new Empty(),
    };
  }
}

export const usersService = UsersService.getInstance();
