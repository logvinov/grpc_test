import {
  Empty,
  FriendshipAction,
  FriendshipMessage,
  NetworkStreamResponse,
} from '../../proto-typings/social-network_pb';
import { FriendValidator } from '../validators/friend-validator';
import { InternalError, NotFoundError } from '../errors';
import { usersService } from './users-service';

export class FriendshipService {
  friendshipList: Map<string, FriendshipMessage>;
  private static instance: FriendshipService;
  validator: FriendValidator;

  private constructor() {
    this.friendshipList = new Map();
    this.validator = new FriendValidator();
  }

  static getInstance(): FriendshipService {
    if (!FriendshipService.instance) {
      FriendshipService.instance = new FriendshipService();
    }

    return FriendshipService.instance;
  }

  createFriendshipKey(friendshipInitiator: string, friendUsername: string) {
    return `${friendshipInitiator}:${friendUsername}`;
  }

  findAll() {
    return this.friendshipList;
  }

  deleteFriendships(username: string) {
    for (let friendship of this.friendshipList.keys()) {
      friendship.includes(username) && this.friendshipList.delete(friendship);
    }
  }

  add(friendship: FriendshipAction) {
    this.validator.addFriend(friendship);

    const username = friendship.getUsername();
    const friendUsername = friendship.getFriendusername();

    const friendshipExists =
      this.friendshipList.get(this.createFriendshipKey(username, friendUsername)) ||
      this.friendshipList.get(this.createFriendshipKey(friendUsername, username));

    if (!usersService.findOne(username) || !usersService.findOne(friendUsername)) {
      throw new NotFoundError(`User ${username} or Friend ${friendUsername} does not exists`);
    }

    if (friendshipExists) {
      throw new InternalError(`User ${friendUsername} already in friend list of User ${username}`);
    }

    const friendshipMessage = new FriendshipMessage();
    friendshipMessage.setUsername(username);
    friendshipMessage.setFriendusername(friendUsername);

    this.friendshipList.set(this.createFriendshipKey(username, friendUsername), friendshipMessage);

    const networkStreamResponse = new NetworkStreamResponse();
    networkStreamResponse.setFriendshipAdded(friendshipMessage);

    return {
      event: networkStreamResponse,
      response: new Empty(),
    };
  }

  delete(friendship: FriendshipAction) {
    this.validator.deleteFriendship(friendship);

    const username = friendship.getUsername();
    const friendUsername = friendship.getFriendusername();

    const friendshipDeleted =
        this.friendshipList.delete(this.createFriendshipKey(username, friendUsername)) ||
        this.friendshipList.delete(this.createFriendshipKey(friendUsername, username));

    if (!friendshipDeleted) {
      throw new NotFoundError(`User ${username} or Friend ${friendUsername} does not exist`);
    }

    const friendshipMessage = new FriendshipMessage();
    friendshipMessage.setUsername(username);
    friendshipMessage.setFriendusername(friendUsername);

    const networkStreamResponse = new NetworkStreamResponse();
    networkStreamResponse.setFriendshipDeleted(friendshipMessage);

    return {
      event: networkStreamResponse,
      response: new Empty(),
    };
  }
}

export const friendshipService = FriendshipService.getInstance();
