import { AddUserAction, FriendshipAction } from '../../../proto-typings/social-network_pb';
import { InternalError, NotFoundError } from '../../errors';
import { FriendshipService, friendshipService } from '../friendship-service';
import { usersService } from '../users-service';

describe('FriendshipService Class', () => {
  it('should create only one instance of FriendshipService class', () => {
    const firstFriendshipServiceInstance = FriendshipService.getInstance();
    const secondFriendshipServiceInstance = FriendshipService.getInstance();
    expect(firstFriendshipServiceInstance).toEqual(secondFriendshipServiceInstance);
  });
  it('should add a new friendship', () => {
    const friendshipMessage = new FriendshipAction();
    const userAddMessage = new AddUserAction();
    const userOneMoreAddMessage = new AddUserAction();

    userAddMessage.setUsername('john');
    userOneMoreAddMessage.setUsername('jack');

    usersService.add(userAddMessage);
    usersService.add(userOneMoreAddMessage);

    friendshipMessage.setUsername('john');
    friendshipMessage.setFriendusername('jack');

    const response = friendshipService.add(friendshipMessage);
    expect(response.event).toBeDefined;
    expect(response.event).toHaveProperty('getFriendshipAdded');
    expect(friendshipService.findAll().size).toEqual(1);
  });
  it('should return a not found error on friendship creation if users dont exist', () => {
    const friendshipMessage = new FriendshipAction();

    friendshipMessage.setUsername('user_not_exists');
    friendshipMessage.setFriendusername('friend_not_exists');
    let error;

    try {
      friendshipService.add(friendshipMessage);
    } catch (err) {
      error = err;
    }

    expect(error).toBeDefined();
    expect(error).toBeInstanceOf(NotFoundError);
    expect(error).toHaveProperty(
      'message',
      'User user_not_exists or Friend friend_not_exists does not exists',
    );
  });
  it('should return an internal error if user already has specified friend in friend list', () => {
    const friendshipMessage = new FriendshipAction();

    friendshipMessage.setUsername('john');
    friendshipMessage.setFriendusername('jack');
    let error;

    try {
      friendshipService.add(friendshipMessage);
    } catch (err) {
      error = err;
    }

    expect(error).toBeDefined();
    expect(error).toBeInstanceOf(InternalError);
    expect(error).toHaveProperty('message', 'User jack already in friend list of User john');
  });
  it('should delete a friendship', () => {
    const friendshipMessage = new FriendshipAction();

    friendshipMessage.setUsername('john');
    friendshipMessage.setFriendusername('jack');

    const response = friendshipService.delete(friendshipMessage);
    const friendshipFound = friendshipService
      .findAll()
      .get(friendshipService.createFriendshipKey('john', 'jack'));

    expect(response.event).toBeDefined;
    expect(response.event).toHaveProperty('getFriendshipDeleted');
    expect(friendshipFound).toBeUndefined();
  });
  it('should return a not found error on friendship deletion case if users not found', () => {
    const friendshipMessage = new FriendshipAction();

    friendshipMessage.setUsername('user_dont_exist');
    friendshipMessage.setFriendusername('friend_dont_exist');
    let error;

    try {
      friendshipService.delete(friendshipMessage);
    } catch (err) {
      error = err;
    }

    expect(error).toBeDefined();
    expect(error).toBeInstanceOf(NotFoundError);
    expect(error).toHaveProperty(
      'message',
      'User user_dont_exist or Friend friend_dont_exist does not exist',
    );
  });
});
