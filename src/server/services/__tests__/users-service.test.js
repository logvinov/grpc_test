import {
  AddUserAction,
  DeleteUserAction,
  UpdateUserAction,
} from '../../../proto-typings/social-network_pb';
import { UsersService, usersService } from '../users-service';
import { InternalError, NotFoundError } from '../../errors';

describe('UsersService Class', () => {
  it('should create only one instance of UsersService class', () => {
    const firstUsersServiceInstance = UsersService.getInstance();
    const secondUsersServiceInstance = UsersService.getInstance();
    expect(firstUsersServiceInstance).toEqual(secondUsersServiceInstance);
  });
  it('should successfully add a new user', () => {
    const userMessage = new AddUserAction();

    userMessage.setUsername('user');

    const response = usersService.add(userMessage);

    expect(response.event).toBeDefined();
    expect(response.event).toHaveProperty('getUserAdded');
    expect(usersService.findOne('user')).toBeDefined();
  });
  it('should return an error if username was not specified in request', () => {
    const userMessage = new AddUserAction();
    let error;

    try {
      usersService.add(userMessage);
    } catch (err) {
      error = err;
    }

    expect(error).toBeDefined();
    expect(error).toBeInstanceOf(InternalError);
    expect(error).toHaveProperty('message', '"username" is not allowed to be empty');
  });
  it('should successfully update user profile', () => {
    const userCreateMessage = new AddUserAction();
    const userUpdateMessage = new UpdateUserAction();

    userCreateMessage.setUsername('new_user');

    usersService.add(userCreateMessage);

    userUpdateMessage.setUsername('new_user');
    userUpdateMessage.setBio('user story');
    userUpdateMessage.setName('User');

    const response = usersService.update(userUpdateMessage);
    expect(response.event).toBeDefined();
    expect(response.event).toHaveProperty('getUserUpdated');
  });
  it('should return an error if user with specified username was not found', () => {
    const userUpdateMessage = new UpdateUserAction();

    userUpdateMessage.setUsername('some_user');
    userUpdateMessage.setBio('user story');
    userUpdateMessage.setName('Some User');
    let error;

    try {
      usersService.update(userUpdateMessage);
    } catch (err) {
      error = err;
    }

    expect(error).toBeDefined();
    expect(error).toBeInstanceOf(NotFoundError);
    expect(error).toHaveProperty('message', 'User some_user not found');
  });
  it('should successfully delete user', () => {
    const deleteUserMessage = new DeleteUserAction();

    deleteUserMessage.setUsername('new_user');

    const response = usersService.delete(deleteUserMessage);
    const userFound = usersService.findOne('new_user');
    expect(response.event).toBeDefined();
    expect(response.event).toHaveProperty('getUserDeleted');
    expect(userFound).toBeUndefined();
  });
  it('should return an error if user was not found', () => {
    const deleteUserMessage = new DeleteUserAction();

    deleteUserMessage.setUsername('user_needed_to_be_deleted');

    let error;
    try {
      usersService.delete(deleteUserMessage);
    } catch (err) {
      error = err;
    }

    expect(error).toBeDefined();
    expect(error).toBeInstanceOf(NotFoundError);
    expect(error).toHaveProperty('message', 'User user_needed_to_be_deleted not found');
  });
});
