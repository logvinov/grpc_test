import { ServiceError, status } from 'grpc';

class BusinessLogicError implements ServiceError {
  message: string;
  name: string;
  code: status;
  constructor(message: string) {
    this.message = message;
    this.name = 'BusinessLogicError';
    this.code = this.status;
  }

  get status() {
    return status.INTERNAL;
  }

  get error() {
    return this.message;
  }
}

export class AlreadyExistError extends BusinessLogicError {
  constructor(message: string) {
    super(message);
    this.name = 'AlreadyExistError';
    this.code = this.status;
  }
  get status() {
    return status.ALREADY_EXISTS;
  }
}

export class InternalError extends BusinessLogicError {
  constructor(message: string) {
    super(message);
    this.name = 'InternalError';
  }
}

export class NotFoundError extends BusinessLogicError {
  constructor(message: string) {
    super(message);
    this.name = 'NotFoundError';
  }
  get status() {
    return status.NOT_FOUND;
  }
}
