import * as grpc from 'grpc';
import { GRPCServer } from '../grpc-server';

jest.mock('grpc');

describe('GRPCServer Class', () => {
  afterEach(jest.clearAllMocks);

  it('should start the gRPC Server', () => {
    const grpcServer = new GRPCServer();
    grpcServer.start();
    const grpcServerStartMock = grpc.Server.mock.instances[0].start;
    expect(grpcServerStartMock).toHaveBeenCalled();
  });

  it('should add a service to the gRPC Server', () => {
    const grpcServer = new GRPCServer();
    const serviceArg = 'service';
    const handlerArg = {};
    grpcServer.addService(serviceArg, handlerArg);
    const grpcServerAddServiceMock = grpc.Server.mock.instances[0].addService;
    expect(grpcServerAddServiceMock).toHaveBeenCalledWith(serviceArg, handlerArg);
  });

  it('should successfully bind a host with port to the gRPC Server', async () => {
    const grpcServer = new GRPCServer();
    const grpcServerMock = grpc.Server.mock.instances[0];
    grpcServerMock.bindAsync.mockImplementation((address, creds, callback) => {
      callback(null);
    });
    grpc.ServerCredentials.createInsecure.mockReturnValue('Mock credentials');
    await grpcServer.bind(0, 'host');

    expect(grpcServerMock.bindAsync).toHaveBeenCalledWith(
      'host:0',
      'Mock credentials',
      expect.any(Function),
    );
  });

  it('should not bind a host with port to the gRPC Server when something went wrong', async () => {
    const error = new Error('Something went wrong')
    const grpcServer = new GRPCServer();
    const grpcServerMock = grpc.Server.mock.instances[0];
    grpcServerMock.bindAsync.mockImplementation((address, creds, callback) => {
      callback(error);
    });
    grpc.ServerCredentials.createInsecure.mockReturnValue('Mock credentials');

    await expect(() => grpcServer.bind(0, 'host')).rejects.toThrowError(error)
    expect(grpcServerMock.bindAsync).toHaveBeenCalledWith(
        'host:0',
        'Mock credentials',
        expect.any(Function),
    );
  });
});
