import { SocialNetwork } from '../social-network';
import { AddUserAction, NetworkStreamResponse } from '../../../proto-typings/social-network_pb';

describe('SocialNetwork Class', () => {
  it('should notify the specific user connection only once', () => {
    const socialNetwork = new SocialNetwork();

    const call = {};
    call.write = jest.fn();

    socialNetwork.notifyOnce(call);
    expect(call.write).toHaveBeenCalled();
  });

  it('should notify all users connections', () => {
    const socialNetwork = new SocialNetwork();
    const userAdded = new AddUserAction();
    const networkResponse = new NetworkStreamResponse();

    const userCall = {};
    userCall.write = jest.fn();

    const anotherUserCall = {};
    anotherUserCall.write = jest.fn();

    userAdded.setUsername('user');

    socialNetwork.setActive(userCall);
    socialNetwork.setActive(anotherUserCall);

    networkResponse.setUserAdded(userAdded);

    socialNetwork.notify(networkResponse);

    expect(userCall.write).toHaveBeenCalledWith(networkResponse);
    expect(anotherUserCall.write).toHaveBeenCalledWith(networkResponse);
  });
});
