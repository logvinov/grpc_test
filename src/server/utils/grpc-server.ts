import * as grpc from 'grpc';
import { ServiceDefinition } from 'grpc';

const { APP_PORT = 50051, APP_HOST = '0.0.0.0' } = process.env;

export class GRPCServer {
  server: grpc.Server;
  constructor(options?: object) {
    this.server = new grpc.Server(options);
  }

  get instance() {
    return this.server
  }

  start() {
    this.server.start();
  }

  addService<T>(service: ServiceDefinition<T>, handler: T) {
    this.server.addService<T>(service, handler);
  }

  bind(port: number = +APP_PORT, host: string = APP_HOST): Promise<grpc.Server> {
    return new Promise((resolve, reject) => {
      this.server.bindAsync(
        `${host}:${port}`,
        grpc.ServerCredentials.createInsecure(),
        (error: Error | null) => {
          if (error) {
            return reject(error);
          }
          console.log(`gRPC listening on ${host}:${port}`);
          return resolve(this.server);
        },
      );
    });
  }
}
