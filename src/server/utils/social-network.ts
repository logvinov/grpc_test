import { ServerWritableStream } from 'grpc';
import {
  Empty,
  NetworkMessage,
  NetworkStreamResponse,
} from '../../proto-typings/social-network_pb';
import { usersService } from '../services/users-service';
import { friendshipService } from '../services/friendship-service';

export class SocialNetwork {
  activeConnections: Array<ServerWritableStream<Empty>>;
  constructor() {
    this.activeConnections = [];
  }

  setActive(call: ServerWritableStream<Empty>) {
    this.activeConnections.push(call);
  }

  notifyOnce(call: ServerWritableStream<Empty>) {
    const networkResponse = new NetworkStreamResponse();
    const networkMessage = new NetworkMessage();

    networkMessage.setUsersList([...usersService.findAll().values()]);
    networkMessage.setFriendshipList([...friendshipService.findAll().values()]);
    networkResponse.setNetwork(networkMessage);
    call.write(networkResponse);
  }

  notify(networkStreamResponse: NetworkStreamResponse) {
    this.activeConnections.forEach((connection) => {
      connection.write(networkStreamResponse);
    });
  }
}
