import { GRPCRouter } from './grpc-router';
import { GRPCServer } from './utils/grpc-server';

process.on('uncaughtException', (err) => {
  console.error('Error caught.', err);
});

const startService = async () => {
  try {
    const server = new GRPCServer();
    const grpcRouter = new GRPCRouter(server);

    grpcRouter.init();

    await server.bind();
    server.start();
  } catch (err) {
    console.error('Error while starting gRPC connection: ' + err);
  }
};

startService();
