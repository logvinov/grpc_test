import { Schema, ValidationOptions } from '@hapi/joi';
import { InternalError } from '../errors';

export class Validator {
  constructor() {}
  validate<RequestDataType>(schema: Schema, data: RequestDataType, options?: ValidationOptions) {
    const { error } = schema.validate(data, { ...options, allowUnknown: true });
    if (error) {
      const joiError = error.details[0].message;
      throw new InternalError(joiError);
    }
  }
}
