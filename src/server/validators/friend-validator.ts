import * as Joi from '@hapi/joi';
import { FriendshipAction } from '../../proto-typings/social-network_pb';
import { Validator } from './index';

export class FriendValidator extends Validator {
  addFriend(args: FriendshipAction) {
    return this.validate<FriendshipAction.AsObject>(
      Joi.object({
        username: Joi.string().required(),
        friendusername: Joi.string().required(),
      }).required(),
      args.toObject(),
    );
  }

  deleteFriendship(args: FriendshipAction) {
    return this.validate<FriendshipAction.AsObject>(
      Joi.object({
        username: Joi.string().required(),
        friendusername: Joi.string().required(),
      }).required(),
      args.toObject(),
    );
  }
}
