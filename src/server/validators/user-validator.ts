import * as Joi from '@hapi/joi';

import { Validator } from './index';
import {
  AddUserAction,
  DeleteUserAction,
  UpdateUserAction,
} from '../../proto-typings/social-network_pb';

export class UserValidator extends Validator {
  addUser(args: AddUserAction) {
    return this.validate<AddUserAction.AsObject>(
      Joi.object({
        username: Joi.string().required(),
      }).required(),
      args.toObject(),
    );
  }

  updateUser(args: UpdateUserAction) {
    return this.validate<UpdateUserAction.AsObject>(
      Joi.object({
        username: Joi.string().required(),
        bio: Joi.string(),
        name: Joi.string(),
      }).required(),
      args.toObject(),
    );
  }

  deleteUser(args: DeleteUserAction) {
    return this.validate<DeleteUserAction.AsObject>(
      Joi.object({
        username: Joi.string().required(),
      }).required(),
      args.toObject(),
    );
  }
}
