import {
  ISocialNetworkServer,
  SocialNetworkService,
} from '../../proto-typings/social-network_grpc_pb';
import { sendUnaryData, ServerUnaryCall, ServerWritableStream } from 'grpc';
import {
  AddUserAction,
  DeleteUserAction,
  Empty,
  FriendshipAction,
  UpdateUserAction,
} from '../../proto-typings/social-network_pb';
import { SocialNetwork } from '../utils/social-network';
import { usersService } from '../services/users-service';
import { friendshipService } from '../services/friendship-service';

export class SocialNetworkHandler implements ISocialNetworkServer {
  socialNetwork: SocialNetwork;
  constructor(socialNetwork: SocialNetwork) {
    this.socialNetwork = socialNetwork;
  }
  join = (call: ServerWritableStream<Empty>) => {
    this.socialNetwork.setActive(call);
    this.socialNetwork.notifyOnce(call);
  };
  addUser = (call: ServerUnaryCall<AddUserAction>, resolve: sendUnaryData<Empty>) => {
    try {
      const result = usersService.add(call.request);
      this.socialNetwork.notify(result.event);
      return resolve(null, result.response);
    } catch (err) {
      return resolve(err, null);
    }
  };
  addFriend = (call: ServerUnaryCall<FriendshipAction>, resolve: sendUnaryData<Empty>) => {
    try {
      const result = friendshipService.add(call.request);
      this.socialNetwork.notify(result.event);
      return resolve(null, result.response);
    } catch (err) {
      return resolve(err, null);
    }
  };
  updateUser = (call: ServerUnaryCall<UpdateUserAction>, resolve: sendUnaryData<Empty>) => {
    try {
      const result = usersService.update(call.request);
      this.socialNetwork.notify(result.event);
      return resolve(null, result.response);
    } catch (err) {
      return resolve(err, null);
    }
  };
  deleteUser = (call: ServerUnaryCall<DeleteUserAction>, resolve: sendUnaryData<Empty>) => {
    try {
      const result = usersService.delete(call.request);
      this.socialNetwork.notify(result.event);
      return resolve(null, result.response);
    } catch (err) {
      return resolve(err, null);
    }
  };
  deleteFriend = (call: ServerUnaryCall<FriendshipAction>, resolve: sendUnaryData<Empty>) => {
    try {
      const result = friendshipService.delete(call.request);
      this.socialNetwork.notify(result.event);
      return resolve(null, result.response);
    } catch (err) {
      return resolve(err, null);
    }
  };
}

export default {
  service: SocialNetworkService,
  handler: new SocialNetworkHandler(new SocialNetwork()),
};
