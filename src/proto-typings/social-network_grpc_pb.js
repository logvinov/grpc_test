// GENERATED CODE -- DO NOT EDIT!

'use strict';
var grpc = require('grpc');
var social$network_pb = require('./social-network_pb.js');

function serialize_AddUserAction(arg) {
  if (!(arg instanceof social$network_pb.AddUserAction)) {
    throw new Error('Expected argument of type AddUserAction');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_AddUserAction(buffer_arg) {
  return social$network_pb.AddUserAction.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_DeleteUserAction(arg) {
  if (!(arg instanceof social$network_pb.DeleteUserAction)) {
    throw new Error('Expected argument of type DeleteUserAction');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_DeleteUserAction(buffer_arg) {
  return social$network_pb.DeleteUserAction.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_Empty(arg) {
  if (!(arg instanceof social$network_pb.Empty)) {
    throw new Error('Expected argument of type Empty');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_Empty(buffer_arg) {
  return social$network_pb.Empty.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_FriendshipAction(arg) {
  if (!(arg instanceof social$network_pb.FriendshipAction)) {
    throw new Error('Expected argument of type FriendshipAction');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_FriendshipAction(buffer_arg) {
  return social$network_pb.FriendshipAction.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_NetworkStreamResponse(arg) {
  if (!(arg instanceof social$network_pb.NetworkStreamResponse)) {
    throw new Error('Expected argument of type NetworkStreamResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_NetworkStreamResponse(buffer_arg) {
  return social$network_pb.NetworkStreamResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_UpdateUserAction(arg) {
  if (!(arg instanceof social$network_pb.UpdateUserAction)) {
    throw new Error('Expected argument of type UpdateUserAction');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_UpdateUserAction(buffer_arg) {
  return social$network_pb.UpdateUserAction.deserializeBinary(new Uint8Array(buffer_arg));
}


var SocialNetworkService = exports.SocialNetworkService = {
  join: {
    path: '/SocialNetwork/Join',
    requestStream: false,
    responseStream: true,
    requestType: social$network_pb.Empty,
    responseType: social$network_pb.NetworkStreamResponse,
    requestSerialize: serialize_Empty,
    requestDeserialize: deserialize_Empty,
    responseSerialize: serialize_NetworkStreamResponse,
    responseDeserialize: deserialize_NetworkStreamResponse,
  },
  addUser: {
    path: '/SocialNetwork/AddUser',
    requestStream: false,
    responseStream: false,
    requestType: social$network_pb.AddUserAction,
    responseType: social$network_pb.Empty,
    requestSerialize: serialize_AddUserAction,
    requestDeserialize: deserialize_AddUserAction,
    responseSerialize: serialize_Empty,
    responseDeserialize: deserialize_Empty,
  },
  addFriend: {
    path: '/SocialNetwork/AddFriend',
    requestStream: false,
    responseStream: false,
    requestType: social$network_pb.FriendshipAction,
    responseType: social$network_pb.Empty,
    requestSerialize: serialize_FriendshipAction,
    requestDeserialize: deserialize_FriendshipAction,
    responseSerialize: serialize_Empty,
    responseDeserialize: deserialize_Empty,
  },
  updateUser: {
    path: '/SocialNetwork/UpdateUser',
    requestStream: false,
    responseStream: false,
    requestType: social$network_pb.UpdateUserAction,
    responseType: social$network_pb.Empty,
    requestSerialize: serialize_UpdateUserAction,
    requestDeserialize: deserialize_UpdateUserAction,
    responseSerialize: serialize_Empty,
    responseDeserialize: deserialize_Empty,
  },
  deleteFriend: {
    path: '/SocialNetwork/DeleteFriend',
    requestStream: false,
    responseStream: false,
    requestType: social$network_pb.FriendshipAction,
    responseType: social$network_pb.Empty,
    requestSerialize: serialize_FriendshipAction,
    requestDeserialize: deserialize_FriendshipAction,
    responseSerialize: serialize_Empty,
    responseDeserialize: deserialize_Empty,
  },
  deleteUser: {
    path: '/SocialNetwork/DeleteUser',
    requestStream: false,
    responseStream: false,
    requestType: social$network_pb.DeleteUserAction,
    responseType: social$network_pb.Empty,
    requestSerialize: serialize_DeleteUserAction,
    requestDeserialize: deserialize_DeleteUserAction,
    responseSerialize: serialize_Empty,
    responseDeserialize: deserialize_Empty,
  },
};

exports.SocialNetworkClient = grpc.makeGenericClientConstructor(SocialNetworkService);
