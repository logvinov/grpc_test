// package: 
// file: social-network.proto

/* tslint:disable */
/* eslint-disable */

import * as grpc from "grpc";
import * as social_network_pb from "./social-network_pb";

interface ISocialNetworkService extends grpc.ServiceDefinition<grpc.UntypedServiceImplementation> {
    join: ISocialNetworkService_IJoin;
    addUser: ISocialNetworkService_IAddUser;
    addFriend: ISocialNetworkService_IAddFriend;
    updateUser: ISocialNetworkService_IUpdateUser;
    deleteFriend: ISocialNetworkService_IDeleteFriend;
    deleteUser: ISocialNetworkService_IDeleteUser;
}

interface ISocialNetworkService_IJoin extends grpc.MethodDefinition<social_network_pb.Empty, social_network_pb.NetworkStreamResponse> {
    path: string; // "/.SocialNetwork/Join"
    requestStream: boolean; // false
    responseStream: boolean; // true
    requestSerialize: grpc.serialize<social_network_pb.Empty>;
    requestDeserialize: grpc.deserialize<social_network_pb.Empty>;
    responseSerialize: grpc.serialize<social_network_pb.NetworkStreamResponse>;
    responseDeserialize: grpc.deserialize<social_network_pb.NetworkStreamResponse>;
}
interface ISocialNetworkService_IAddUser extends grpc.MethodDefinition<social_network_pb.AddUserAction, social_network_pb.Empty> {
    path: string; // "/.SocialNetwork/AddUser"
    requestStream: boolean; // false
    responseStream: boolean; // false
    requestSerialize: grpc.serialize<social_network_pb.AddUserAction>;
    requestDeserialize: grpc.deserialize<social_network_pb.AddUserAction>;
    responseSerialize: grpc.serialize<social_network_pb.Empty>;
    responseDeserialize: grpc.deserialize<social_network_pb.Empty>;
}
interface ISocialNetworkService_IAddFriend extends grpc.MethodDefinition<social_network_pb.FriendshipAction, social_network_pb.Empty> {
    path: string; // "/.SocialNetwork/AddFriend"
    requestStream: boolean; // false
    responseStream: boolean; // false
    requestSerialize: grpc.serialize<social_network_pb.FriendshipAction>;
    requestDeserialize: grpc.deserialize<social_network_pb.FriendshipAction>;
    responseSerialize: grpc.serialize<social_network_pb.Empty>;
    responseDeserialize: grpc.deserialize<social_network_pb.Empty>;
}
interface ISocialNetworkService_IUpdateUser extends grpc.MethodDefinition<social_network_pb.UpdateUserAction, social_network_pb.Empty> {
    path: string; // "/.SocialNetwork/UpdateUser"
    requestStream: boolean; // false
    responseStream: boolean; // false
    requestSerialize: grpc.serialize<social_network_pb.UpdateUserAction>;
    requestDeserialize: grpc.deserialize<social_network_pb.UpdateUserAction>;
    responseSerialize: grpc.serialize<social_network_pb.Empty>;
    responseDeserialize: grpc.deserialize<social_network_pb.Empty>;
}
interface ISocialNetworkService_IDeleteFriend extends grpc.MethodDefinition<social_network_pb.FriendshipAction, social_network_pb.Empty> {
    path: string; // "/.SocialNetwork/DeleteFriend"
    requestStream: boolean; // false
    responseStream: boolean; // false
    requestSerialize: grpc.serialize<social_network_pb.FriendshipAction>;
    requestDeserialize: grpc.deserialize<social_network_pb.FriendshipAction>;
    responseSerialize: grpc.serialize<social_network_pb.Empty>;
    responseDeserialize: grpc.deserialize<social_network_pb.Empty>;
}
interface ISocialNetworkService_IDeleteUser extends grpc.MethodDefinition<social_network_pb.DeleteUserAction, social_network_pb.Empty> {
    path: string; // "/.SocialNetwork/DeleteUser"
    requestStream: boolean; // false
    responseStream: boolean; // false
    requestSerialize: grpc.serialize<social_network_pb.DeleteUserAction>;
    requestDeserialize: grpc.deserialize<social_network_pb.DeleteUserAction>;
    responseSerialize: grpc.serialize<social_network_pb.Empty>;
    responseDeserialize: grpc.deserialize<social_network_pb.Empty>;
}

export const SocialNetworkService: ISocialNetworkService;

export interface ISocialNetworkServer {
    join: grpc.handleServerStreamingCall<social_network_pb.Empty, social_network_pb.NetworkStreamResponse>;
    addUser: grpc.handleUnaryCall<social_network_pb.AddUserAction, social_network_pb.Empty>;
    addFriend: grpc.handleUnaryCall<social_network_pb.FriendshipAction, social_network_pb.Empty>;
    updateUser: grpc.handleUnaryCall<social_network_pb.UpdateUserAction, social_network_pb.Empty>;
    deleteFriend: grpc.handleUnaryCall<social_network_pb.FriendshipAction, social_network_pb.Empty>;
    deleteUser: grpc.handleUnaryCall<social_network_pb.DeleteUserAction, social_network_pb.Empty>;
}

export interface ISocialNetworkClient {
    join(request: social_network_pb.Empty, options?: Partial<grpc.CallOptions>): grpc.ClientReadableStream<social_network_pb.NetworkStreamResponse>;
    join(request: social_network_pb.Empty, metadata?: grpc.Metadata, options?: Partial<grpc.CallOptions>): grpc.ClientReadableStream<social_network_pb.NetworkStreamResponse>;
    addUser(request: social_network_pb.AddUserAction, callback: (error: grpc.ServiceError | null, response: social_network_pb.Empty) => void): grpc.ClientUnaryCall;
    addUser(request: social_network_pb.AddUserAction, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: social_network_pb.Empty) => void): grpc.ClientUnaryCall;
    addUser(request: social_network_pb.AddUserAction, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: social_network_pb.Empty) => void): grpc.ClientUnaryCall;
    addFriend(request: social_network_pb.FriendshipAction, callback: (error: grpc.ServiceError | null, response: social_network_pb.Empty) => void): grpc.ClientUnaryCall;
    addFriend(request: social_network_pb.FriendshipAction, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: social_network_pb.Empty) => void): grpc.ClientUnaryCall;
    addFriend(request: social_network_pb.FriendshipAction, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: social_network_pb.Empty) => void): grpc.ClientUnaryCall;
    updateUser(request: social_network_pb.UpdateUserAction, callback: (error: grpc.ServiceError | null, response: social_network_pb.Empty) => void): grpc.ClientUnaryCall;
    updateUser(request: social_network_pb.UpdateUserAction, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: social_network_pb.Empty) => void): grpc.ClientUnaryCall;
    updateUser(request: social_network_pb.UpdateUserAction, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: social_network_pb.Empty) => void): grpc.ClientUnaryCall;
    deleteFriend(request: social_network_pb.FriendshipAction, callback: (error: grpc.ServiceError | null, response: social_network_pb.Empty) => void): grpc.ClientUnaryCall;
    deleteFriend(request: social_network_pb.FriendshipAction, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: social_network_pb.Empty) => void): grpc.ClientUnaryCall;
    deleteFriend(request: social_network_pb.FriendshipAction, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: social_network_pb.Empty) => void): grpc.ClientUnaryCall;
    deleteUser(request: social_network_pb.DeleteUserAction, callback: (error: grpc.ServiceError | null, response: social_network_pb.Empty) => void): grpc.ClientUnaryCall;
    deleteUser(request: social_network_pb.DeleteUserAction, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: social_network_pb.Empty) => void): grpc.ClientUnaryCall;
    deleteUser(request: social_network_pb.DeleteUserAction, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: social_network_pb.Empty) => void): grpc.ClientUnaryCall;
}

export class SocialNetworkClient extends grpc.Client implements ISocialNetworkClient {
    constructor(address: string, credentials: grpc.ChannelCredentials, options?: object);
    public join(request: social_network_pb.Empty, options?: Partial<grpc.CallOptions>): grpc.ClientReadableStream<social_network_pb.NetworkStreamResponse>;
    public join(request: social_network_pb.Empty, metadata?: grpc.Metadata, options?: Partial<grpc.CallOptions>): grpc.ClientReadableStream<social_network_pb.NetworkStreamResponse>;
    public addUser(request: social_network_pb.AddUserAction, callback: (error: grpc.ServiceError | null, response: social_network_pb.Empty) => void): grpc.ClientUnaryCall;
    public addUser(request: social_network_pb.AddUserAction, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: social_network_pb.Empty) => void): grpc.ClientUnaryCall;
    public addUser(request: social_network_pb.AddUserAction, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: social_network_pb.Empty) => void): grpc.ClientUnaryCall;
    public addFriend(request: social_network_pb.FriendshipAction, callback: (error: grpc.ServiceError | null, response: social_network_pb.Empty) => void): grpc.ClientUnaryCall;
    public addFriend(request: social_network_pb.FriendshipAction, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: social_network_pb.Empty) => void): grpc.ClientUnaryCall;
    public addFriend(request: social_network_pb.FriendshipAction, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: social_network_pb.Empty) => void): grpc.ClientUnaryCall;
    public updateUser(request: social_network_pb.UpdateUserAction, callback: (error: grpc.ServiceError | null, response: social_network_pb.Empty) => void): grpc.ClientUnaryCall;
    public updateUser(request: social_network_pb.UpdateUserAction, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: social_network_pb.Empty) => void): grpc.ClientUnaryCall;
    public updateUser(request: social_network_pb.UpdateUserAction, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: social_network_pb.Empty) => void): grpc.ClientUnaryCall;
    public deleteFriend(request: social_network_pb.FriendshipAction, callback: (error: grpc.ServiceError | null, response: social_network_pb.Empty) => void): grpc.ClientUnaryCall;
    public deleteFriend(request: social_network_pb.FriendshipAction, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: social_network_pb.Empty) => void): grpc.ClientUnaryCall;
    public deleteFriend(request: social_network_pb.FriendshipAction, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: social_network_pb.Empty) => void): grpc.ClientUnaryCall;
    public deleteUser(request: social_network_pb.DeleteUserAction, callback: (error: grpc.ServiceError | null, response: social_network_pb.Empty) => void): grpc.ClientUnaryCall;
    public deleteUser(request: social_network_pb.DeleteUserAction, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: social_network_pb.Empty) => void): grpc.ClientUnaryCall;
    public deleteUser(request: social_network_pb.DeleteUserAction, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: social_network_pb.Empty) => void): grpc.ClientUnaryCall;
}
