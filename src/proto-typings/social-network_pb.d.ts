// package: 
// file: social-network.proto

/* tslint:disable */
/* eslint-disable */

import * as jspb from "google-protobuf";

export class UserMessage extends jspb.Message { 
    getUsername(): string;
    setUsername(value: string): void;

    getBio(): string;
    setBio(value: string): void;

    getName(): string;
    setName(value: string): void;

    getCreatedat(): number;
    setCreatedat(value: number): void;

    getUpdatedat(): number;
    setUpdatedat(value: number): void;


    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): UserMessage.AsObject;
    static toObject(includeInstance: boolean, msg: UserMessage): UserMessage.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: UserMessage, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): UserMessage;
    static deserializeBinaryFromReader(message: UserMessage, reader: jspb.BinaryReader): UserMessage;
}

export namespace UserMessage {
    export type AsObject = {
        username: string,
        bio: string,
        name: string,
        createdat: number,
        updatedat: number,
    }
}

export class AddUserAction extends jspb.Message { 
    getUsername(): string;
    setUsername(value: string): void;


    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): AddUserAction.AsObject;
    static toObject(includeInstance: boolean, msg: AddUserAction): AddUserAction.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: AddUserAction, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): AddUserAction;
    static deserializeBinaryFromReader(message: AddUserAction, reader: jspb.BinaryReader): AddUserAction;
}

export namespace AddUserAction {
    export type AsObject = {
        username: string,
    }
}

export class UpdateUserAction extends jspb.Message { 
    getUsername(): string;
    setUsername(value: string): void;

    getBio(): string;
    setBio(value: string): void;

    getName(): string;
    setName(value: string): void;


    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): UpdateUserAction.AsObject;
    static toObject(includeInstance: boolean, msg: UpdateUserAction): UpdateUserAction.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: UpdateUserAction, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): UpdateUserAction;
    static deserializeBinaryFromReader(message: UpdateUserAction, reader: jspb.BinaryReader): UpdateUserAction;
}

export namespace UpdateUserAction {
    export type AsObject = {
        username: string,
        bio: string,
        name: string,
    }
}

export class DeleteUserAction extends jspb.Message { 
    getUsername(): string;
    setUsername(value: string): void;


    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): DeleteUserAction.AsObject;
    static toObject(includeInstance: boolean, msg: DeleteUserAction): DeleteUserAction.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: DeleteUserAction, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): DeleteUserAction;
    static deserializeBinaryFromReader(message: DeleteUserAction, reader: jspb.BinaryReader): DeleteUserAction;
}

export namespace DeleteUserAction {
    export type AsObject = {
        username: string,
    }
}

export class FriendshipMessage extends jspb.Message { 
    getUsername(): string;
    setUsername(value: string): void;

    getFriendusername(): string;
    setFriendusername(value: string): void;


    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): FriendshipMessage.AsObject;
    static toObject(includeInstance: boolean, msg: FriendshipMessage): FriendshipMessage.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: FriendshipMessage, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): FriendshipMessage;
    static deserializeBinaryFromReader(message: FriendshipMessage, reader: jspb.BinaryReader): FriendshipMessage;
}

export namespace FriendshipMessage {
    export type AsObject = {
        username: string,
        friendusername: string,
    }
}

export class FriendshipAction extends jspb.Message { 
    getUsername(): string;
    setUsername(value: string): void;

    getFriendusername(): string;
    setFriendusername(value: string): void;


    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): FriendshipAction.AsObject;
    static toObject(includeInstance: boolean, msg: FriendshipAction): FriendshipAction.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: FriendshipAction, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): FriendshipAction;
    static deserializeBinaryFromReader(message: FriendshipAction, reader: jspb.BinaryReader): FriendshipAction;
}

export namespace FriendshipAction {
    export type AsObject = {
        username: string,
        friendusername: string,
    }
}

export class NetworkMessage extends jspb.Message { 
    clearUsersList(): void;
    getUsersList(): Array<UserMessage>;
    setUsersList(value: Array<UserMessage>): void;
    addUsers(value?: UserMessage, index?: number): UserMessage;

    clearFriendshipList(): void;
    getFriendshipList(): Array<FriendshipMessage>;
    setFriendshipList(value: Array<FriendshipMessage>): void;
    addFriendship(value?: FriendshipMessage, index?: number): FriendshipMessage;


    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): NetworkMessage.AsObject;
    static toObject(includeInstance: boolean, msg: NetworkMessage): NetworkMessage.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: NetworkMessage, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): NetworkMessage;
    static deserializeBinaryFromReader(message: NetworkMessage, reader: jspb.BinaryReader): NetworkMessage;
}

export namespace NetworkMessage {
    export type AsObject = {
        usersList: Array<UserMessage.AsObject>,
        friendshipList: Array<FriendshipMessage.AsObject>,
    }
}

export class NetworkStreamResponse extends jspb.Message { 

    hasUserAdded(): boolean;
    clearUserAdded(): void;
    getUserAdded(): UserMessage | undefined;
    setUserAdded(value?: UserMessage): void;


    hasFriendshipAdded(): boolean;
    clearFriendshipAdded(): void;
    getFriendshipAdded(): FriendshipMessage | undefined;
    setFriendshipAdded(value?: FriendshipMessage): void;


    hasFriendshipDeleted(): boolean;
    clearFriendshipDeleted(): void;
    getFriendshipDeleted(): FriendshipMessage | undefined;
    setFriendshipDeleted(value?: FriendshipMessage): void;


    hasUserUpdated(): boolean;
    clearUserUpdated(): void;
    getUserUpdated(): UserMessage | undefined;
    setUserUpdated(value?: UserMessage): void;


    hasUserDeleted(): boolean;
    clearUserDeleted(): void;
    getUserDeleted(): UserMessage | undefined;
    setUserDeleted(value?: UserMessage): void;


    hasNetwork(): boolean;
    clearNetwork(): void;
    getNetwork(): NetworkMessage | undefined;
    setNetwork(value?: NetworkMessage): void;


    getDataCase(): NetworkStreamResponse.DataCase;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): NetworkStreamResponse.AsObject;
    static toObject(includeInstance: boolean, msg: NetworkStreamResponse): NetworkStreamResponse.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: NetworkStreamResponse, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): NetworkStreamResponse;
    static deserializeBinaryFromReader(message: NetworkStreamResponse, reader: jspb.BinaryReader): NetworkStreamResponse;
}

export namespace NetworkStreamResponse {
    export type AsObject = {
        userAdded?: UserMessage.AsObject,
        friendshipAdded?: FriendshipMessage.AsObject,
        friendshipDeleted?: FriendshipMessage.AsObject,
        userUpdated?: UserMessage.AsObject,
        userDeleted?: UserMessage.AsObject,
        network?: NetworkMessage.AsObject,
    }

    export enum DataCase {
        DATA_NOT_SET = 0,
    
    USER_ADDED = 1,

    FRIENDSHIP_ADDED = 2,

    FRIENDSHIP_DELETED = 3,

    USER_UPDATED = 4,

    USER_DELETED = 5,

    NETWORK = 6,

    }

}

export class Empty extends jspb.Message { 

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Empty.AsObject;
    static toObject(includeInstance: boolean, msg: Empty): Empty.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Empty, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Empty;
    static deserializeBinaryFromReader(message: Empty, reader: jspb.BinaryReader): Empty;
}

export namespace Empty {
    export type AsObject = {
    }
}
