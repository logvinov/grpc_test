FROM node:12.16.2

RUN npm install -g pm2
ENV NODE_ENV=production

WORKDIR /home/app

COPY package.json .

ADD /dist /home/app/dist
RUN npm install

EXPOSE 50051