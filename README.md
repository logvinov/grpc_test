# GRPC Social Network Solution
Test tasks solutions based on GRPC approach

## Project Description
The project is a social network which contains with users (as nodes) and friendships (as edges).

Social Network allows creating, updating, deleting the Users and their Friendships.

## Project Setup
1. Clone the repository
2. Install dependencies via command below
   ```bash
   npm install
   ```
3. Configure environment

    The app uses a .env configuration file for most settings, see what is required and what are the defaults.
    
4. Run server in dev mode
   ```bash
   npm run dev:server
   ```
5. Now we can run the test setup script to upload some set of users data
   ```bash
   npm run test-setup
   ```
   
## Test Setup

There are several prepared instances of client that can listen to changes from test setup script which has been already mentioned in paragraph 5 of Project Setup category.

To run some of prepared instances, please specify the `CLIENT` variable as a name of the `client-instance` file and run command below
   ```bash
   CLIENT=client_filename npm run client:start
   ```
For example, let's run the already prepared instance of client `client-inst1.ts`
   ```bash
   CLIENT=client-inst1 npm run client:start
   ```
Now we are listening the changes from all connected clients. Try it out!

## Unit-testing
To run unit tests, please use command below
   ```bash
   npm run test
   ```

## Docker Setup
To build a docker container with the server setups please run
   ```bash
   npm run docker:build
   ```
First of all command above will run unit tests and only if tests successfully passed docker starts to build the container


Then we easily can up the container in a daemon process by command below
   ```bash
   docker-compose up -d
   ```
We also can check the logs by command
   ```bash
   docker-compose logs -f
   ```
NOTE: The server runs in a cluster mode inside of docker container by pm2 module instead of nodemon as in the dev environment.

The pm2 module helps us with vertically scaling of our server.
