#!/usr/bin/env bash

BASEDIR=$(dirname "$0")
cd "${BASEDIR}"/../

GRPC_TOOLS_NODE_PROTOC_PLUGIN="./node_modules/.bin/grpc_tools_node_protoc_plugin"
GRPC_TOOLS_NODE_PROTOC_MODULE="./node_modules/.bin/grpc_tools_node_protoc"
PROTOC_GEN_TS_MODULE_PATH="./node_modules/.bin/protoc-gen-ts"
TYPINGS_DIR_PATH="./src/proto-typings"
PROTO_BASEDIR="proto"

# create typings directories
mkdir -p ${TYPINGS_DIR_PATH}

for f in ./proto/*; do

  # skip the non protos files
  if [ "$(basename "$f")" == "index.ts" ]; then
      continue
  fi

  # JavaScript code generation
  ${GRPC_TOOLS_NODE_PROTOC_MODULE} \
    --js_out=import_style=commonjs,binary:${TYPINGS_DIR_PATH} \
    --grpc_out=${TYPINGS_DIR_PATH} \
    --plugin=protoc-gen-grpc=${GRPC_TOOLS_NODE_PROTOC_PLUGIN} \
    -I "./${PROTO_BASEDIR}" \
      ${PROTO_BASEDIR}/*.proto

  # generate d.ts codes
  ${GRPC_TOOLS_NODE_PROTOC_MODULE} \
      --plugin=protoc-gen-ts="${PROTOC_GEN_TS_MODULE_PATH}" \
      --ts_out=${TYPINGS_DIR_PATH} \
      -I "./${PROTO_BASEDIR}" \
      ${PROTO_BASEDIR}/*.proto

done